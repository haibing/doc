# 常见问题

### 如何修改直播观看人数

直播间-管理-数据统计：

![常见问题-2020-12-08-20201208151809](https://images5doc.oss-cn-beijing.aliyuncs.com/images/常见问题-2020-12-08-20201208151809.png)

### ios打开提示“未受信任的企业级开发者”怎么解决

[https://jingyan.baidu.com/article/ae97a64679af7dfbfc461d5a.html](https://jingyan.baidu.com/article/ae97a64679af7dfbfc461d5a.html)