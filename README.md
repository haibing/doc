## 快速开始
### 创建直播间

- pc端点击`直播间`管理，`创建直播间`按钮，输入直播间名称。

    ![README-2020-08-30-20200830010413](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-08-30-20200830010413.png)

- APP端，点击右下角`＋`按钮，创建直播间。

    ![README-2020-09-02-20200902131632](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-09-02-20200902131632.png ':size=45%')

### 如何开始直播

- pc端点击直播间操作下的`管理`按钮，点击`查看`流地址，复制推流地址。使用[OBS](OBS推流工具)或者硬件编码器进行推流。

    ![README-2020-08-30-20200830011121](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-08-30-20200830011121.png)

- APP端点击直播间下面【直播】按钮，即可进入直播推流界面。

    ![README-2020-09-02-20200902131713](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-09-02-20200902131713.png ':size=45%')

### 如何观看直播

- pc端点击`播控`按钮，`开启观看直播页`.

    ![README-2020-08-30-20200830012003](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-08-30-20200830012003.png)
    ![README-2020-08-30-20200830011857](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-08-30-20200830011857.png)

    点击`查看`直播间地址，获取`微信菜单地址`及`APP菜单地址`。
    
    ![README-2020-08-30-20200830012152](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-08-30-20200830012152.png ':size=45%')

?> 微信菜单地址可以直接通过微信聊天窗口发送观看，也可以添加到公众号自定义菜单。微信菜单地址只能在微信客户端打开。
APP菜单地址可用于移动端打开，也可以在微信客户端打开，跟微信菜单地址的区别是无法获取微信用户信息。

- 飞流直播APP分享观看页到微信。

    ![README-2020-09-02-20200902131915](https://images5doc.oss-cn-beijing.aliyuncs.com/images/README-2020-09-02-20200902131915.png ':size=45%')


?> 也可以通过[VLC播放器](VLCplayer)播放预览。