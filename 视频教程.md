# 视频教程

## 网页端操作
[pc后台操作](https://hbrbvod.chinamcache.com/hbrbsy/vod/2020/09/02/0c66a42f15934090944bc440b07fcc91/0c66a42f15934090944bc440b07fcc91_h264_1000k_mp4.mp4 ':include controls="true" width=80%')

## 飞流直播APP操作

[APP操作](https://hbrbvod.chinamcache.com/hbrbsy/vod/2020/09/02/a838b44cc5ce4b659faf6ad8c4402cb3/a838b44cc5ce4b659faf6ad8c4402cb3_h264_1000k_mp4.mp4 ':include')

## 云导播台操作
[云导播台操作](https://hbrbvod.chinamcache.com/hbrbsy/vod/2020/09/02/100d429c592d460ca950c12bc587d9cf/100d429c592d460ca950c12bc587d9cf_h264_1000k_mp4.mp4 ':include controls="true" width=80%')