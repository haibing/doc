## 简介
VLC media player 是一款自由、开源的跨平台多媒体播放器及框架，可用来播放大多数多媒体文件，以及DVD、音频CD、VCD和各类流媒体协议。

## VLC media player 是否提供源代码下载？
是的，VLC media player可以提供源代码下载。

## VLC media player使用是否收费？
个人使用并不收费，这个程序和其源代码都是免费的。企业使用请参考VLC media player的相关使用协议。

## VLC media player支持哪些操作系统？
支持 OS X、Windows、Linux、iOS、Android、Chrome OS等操作系统。在各种操作系统上都有相应的播放程序。

## VLC media player支持哪些直播格式？
常见的直播格式都支持，包括RTMP格式、FLV格式以及M3U8格式。

## 下载VLC media player
请您到 [VLC media player官网](https://www.videolan.org/vlc/) 下载最新软件 。

## 使用VLC media player播放视频
请您在安装好VLC media player之后，按照如下步骤操作进行视频播放：

1. 在主界面选择 **媒体** > **打开网络串流**。

    ![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/52142/cn_zh/1532337244174/vlc%201.png)

2. 获取直播地址。

    您可以在 **飞流直播控制台** > **直播间管理** > **机位** 中获取 **播流地址**。如下图所示：

    ![VLCpalyer-2020-10-13-20201013142206](https://images5doc.oss-cn-beijing.aliyuncs.com/images/VLCpalyer-2020-10-13-20201013142206.png)

3. 在弹出对话框中输入 直播播流地址，并单击 播放。

    ![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/52142/cn_zh/1532337398891/vlc%202.png)

    您可以开始观看直播。

!> 注意：缓冲时间和网络有关，正常在2-5秒左右。

更多问题，请您查看 [VLC media player官网](https://www.videolan.org/)。

> 原文地址：<https://help.aliyun.com/knowledge_detail/52142.html>