
# 简介
## 什么是OBS？

Open Broadcaster Software（简称OBS）是一款好用的直播流媒体内容制作软件。

## OBS使用是否收费？

不收费，这个程序和其源代码都是免费的。

## OBS都支持哪些操作系统？

支持 OS X、Windows、Linux操作系统，适用于多种直播场景，满足大部分直播行为的操作需求。

# 下载OBS

请您到OBS官网下载最新软件 [OBS官方下载](https://obsproject.com/download)。

![OBS推流工具-2020-08-29-20200829181253](https://images5doc.oss-cn-beijing.aliyuncs.com/images/OBS推流工具-2020-08-29-20200829181253.png)
# 设置OBS

## 通用设置
![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519370947698/obs%E8%AE%BE%E7%BD%AE.png)

如果您有直播录制需求，由于直播时网络环境较复杂，建议您进行直播时，在本地进行录制备份。

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519370962975/obs%E8%AE%BE%E7%BD%AE2.png)


## 流设置

![OBS推流工具-2020-10-09-20201009133607](https://images5doc.oss-cn-beijing.aliyuncs.com/images/OBS推流工具-2020-10-09-20201009133607.png)

!> 注意：将飞流直播机位的推流地址分两部分输入 URL 与 流名称 中。URL 部分填写推流流地址的前两部分，流名称 部分填写推流地址的最后一段。

实际Mac版OBS的流名称与Windows版OBS的流名称中的内容默认不显示明文。

以上面的推流地址为例，参数设置为：

FMS URL / URL： `rtmp://pushpgc.chinamcache.com/pgc/`

流秘钥: `3225f7757a868fee45d038cxxxxxxxx`


## 分辨率设置

输出分辨率就是您想让观众看到的画面分辨率，分辨率决定着画面的清晰程度，越高画面越清晰。

FPS是视频帧率，它控制视频观看视频流畅，普通视频帧率有24-30帧，低于16帧画面看起来有卡顿感。而游戏对帧率要求比较高，一般小于30帧游戏会显得不连贯。

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1479882925187/DingTalk20161123143345.png)

常见视频分辨率与比例对照表

|比例|	分辨率|	比例|	分辨率|	比例|	分辨率|
|------|---|---|---|-----|---|
|4:03|	2048x1536 1600x1200 1400x1050 1152×864 1024x768 800x600 640x480|	16:09|	1920x1080 1600×900 280x720|	16:10	|2560x1600 1920x1200 1680x1050 1440x900 1280x800 1366x768 960x600 800x480|

## 输出设置
![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519453907641/obs%E8%BE%93%E5%87%BA%E8%AE%BE%E7%BD%AE.png)

!>注意：视频或音频比特率越大，所需的带宽越大。

## 减少直播延迟设置

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519454253714/obs%E8%BE%93%E5%87%BA%E8%AE%BE%E7%BD%AE%20%E9%AB%98%E7%BA%A7.png)

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519454279544/%E5%90%AF%E7%94%A8.png)


# 常见操作设置介绍
## 直播来源设置介绍

OBS工具支持简单的内容快速编辑、剪辑、场景、来源素材快速切换的操作，可以对直播活动进行快速简单的内容编辑。

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519454580279/Image%2010.png)

- **BrowserSource：** 可实时展示一个网络页面。

- **图像：** 适用于单张图像直播。

- **图像幻灯片放映：** 可循环或者顺序多张播放图片，适用于会议、教育类课件直播。

- **场景：** 实现各种强大的直播效果。此时，另一个场景是作为来源被添加进当前场景的，可以实现整个场景的插入。

- **媒体源：** 可上传本地视频，并本地点播视频文件进行直播化处理。

- **文本：** 实时添加文字在直播窗口中。

- **显示器捕获：** 可实时动态捕捉您电脑桌面的操作，电脑桌面中所有的操作均执行直播。

- **游戏捕获：** 允许对指定来源的游戏进行直播。适用于大小游戏的实况直播。

- **窗口捕获：** 可根据您选择的窗口进行实时动态捕获，使用窗口捕获的好处是直播仅显示您当前窗口的内容，其他窗口不会进行直播捕获。适用于软件讲解或操作指引类的直播。

- **色源：** 使用这个来源可以添加一个色块到你的场景中，作为一个背景色。该色块可以调节透明度，成为全屏透明色彩。

- **视频捕获设备：** 实时动态捕捉摄像设备，可将摄像后的画面进行直播。常见场景有娱乐秀场，视频会议等。

- **音频输入捕获：** 用于音频直播活动（音频输入设备）。

- **音频输出捕获：** 用于音频直播活动（音频输出设备）。

!>场景与来源内容可根据实际需要进行切换，叠加展示。
# 工作室模式介绍


工作室模式提供给用户可对当前直播的内容进行实时编辑的操作的界面。

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519455086005/%E6%93%8D%E4%BD%9C%E7%95%8C%E9%9D%A2.png)

- 左边画面为素材准备界面，右侧画面为直播显示画面。

- 先设置画面转换模式。

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519455410289/%E8%BF%87%E5%BA%A6.png)

- 画面需要进行切换时，分别执行过渡动画设置。

- 快速过渡动画可在场景过渡中设置。

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519455434554/%E8%AE%BE%E7%BD%AE.png)

# 常见问题

- 为什么使用OBS做直播推流有卡顿，丢帧的现象？

  1. OBS有丢帧监测数据，发现有丢帧现象时，请您首先检查带宽资源是否够用，上传速度是否满足直播需求。同时尝试适当降低视频分辨率。

  2. 查看您的CPU是否耗费较高。电脑配置过低，电脑资源被占满，也会影响视频的流畅度。

![](https://docs-aliyun.cn-hangzhou.oss.aliyun-inc.com/assets/pic/45212/cn_zh/1519455739126/%E4%B8%A2%E5%B8%A7.png)


更多问题，请您查看 [OBS论坛讨论](http://www.xspliter.com/forum-74-1.html)。

>原文地址：<https://help.aliyun.com/knowledge_detail/45212.html#title-b8n-1py-obs>